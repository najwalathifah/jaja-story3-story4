from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import ActivityForm, PersonForm
from .models import Activity, Person

def indexStory6(request):
    return render(request, 'indexStory6.html')

def addActivity(request):
    form_activity = ActivityForm()

    if request.method == 'POST':
        # print(request.POST)
        form_activity = ActivityForm(request.POST)
        
        if form_activity.is_valid():
            data = form_activity.cleaned_data
            activity = Activity()
            activity.activity_form = data['activity_form']
            activity.activity_desc = data['activity_desc']
            activity.save()
            # Activity.objects.create(
            #     activity_name = request.POST['activity_name'],
            #     activity_desc = request.POST['activity_desc']
            # )
            return redirect('/story6/addActivity')
            
    context = {'form':form_activity.as_table}
    return render(request, 'addActivity.html', context)

def listActivity(request):
    activity = Activity.objects.all()
    person = Person.objects.all()
    # return HttpResponseRedirect('/story6/listActivity')
    return render(request, 'listActivity.html', {'activity': activity, 'person': person})

def registerPerson(request, id_person):
    form_person = PersonForm()

    if request.method == 'POST':
        form_person = PersonForm(request.POST)

        if form_person.is_valid():
            data = form_person.cleaned_data
            person = Person()
            person.person_name = request.POST['person_name']
            person.activity = Activity.objects.get(id=id_person)
            person.save()
            return redirect('/story6/registerPerson')
        
        # else:
        #     return render(request, 'story6/register.html', {'form': form_person, 'status': 'failed'})
    
    # else:
    #     return render(request, 'story6/registerPerson.html', {'form': form_person})

    context = {'form':form_person.as_table}
    return render(request, 'registerPerson.html', context)

# def deletePerson(request, id_persondel):
#     Person.objects.filter(id=id_persondel).delete()
#     return redirect('/story6/listActivity')