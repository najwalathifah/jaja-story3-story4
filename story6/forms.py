from django import forms

class ActivityForm(forms.Form):
    activity_form = forms.CharField(
        label="Nama kegiatan",
        max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60'
            }
        )
    )

    activity_desc = forms.CharField(
        label="Deskripsi kegiatan",
        max_length=200,
        widget=forms.Textarea(
            attrs={
                'class': 'form-control w-60'
            }
        )
    )

class PersonForm(forms.Form):
    person_name = forms.CharField(
        label="Nama",
        max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control w-60'
            }
        )
    )