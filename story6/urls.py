from django.urls import path, include
from . import views

app_name = 'story6'

urlpatterns = [
    path('indexStory6/', views.indexStory6, name='indexStory6'),
    path('addActivity/', views.addActivity, name='addActivity'),
    path('listActivity/', views.listActivity, name='listActivity'),
    path('registerPerson/<int:id_person>/', views.registerPerson, name='registerPerson'),
    # path('delete/<int:id_persondel>/', views.deletePerson, name='deletePerson'),
]
