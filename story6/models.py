from django.db import models

class Activity(models.Model):
    activity_name = models.CharField(max_length=200)
    activity_desc = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)

    # def __str__(self):
    #     return self.activity_name

class Person(models.Model):
    person_name = models.CharField(max_length=200)
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, null=True, blank=True)

    # def __str__(self):
    #     return self.person_name


