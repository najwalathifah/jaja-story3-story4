from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Activity, Person
from .views import indexStory6, addActivity, listActivity, registerPerson
from .forms import ActivityForm, PersonForm

class ModelTest(TestCase):
    def setUp(self):
        self.activity = Activity.objects.create(
        activity_name = "nugas", activity_desc = "Mengerjakan Tugas")
        self.person = Person.objects.create(person_name = "Jaja")

    def test_instance_created(self):
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Person.objects.count(), 1)

    # def test_str(self):
    #     self.assertEqual(str(self.activity), "nugas")
    #     self.assertEqual(str(self.person), "Jaja")

class ViewsTest(TestCase):
    def test_GET_indexStory6(self):
        response = self.client.get('/story6/indexStory6/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'indexStory6.html')

    def test_GET_listActivity(self):
        response = self.client.get('/story6/listActivity/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'listActivity.html')

    def test_GET_addActivity(self):
        response = self.client.get('/story6/addActivity/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'addActivity.html')

    # def test_index_use_right_function(self):
    #     found = resolve(self.register)
    #     self.assertEqual(found.func, indexStory6)

    # def test_listActivity_use_right_function(self):
    #     found = resolve(self.register)
    #     self.assertEqual(found.func, listActivity)

    # def test_addActivity_use_right_function(self):
    #     found = resolve(self.register)
    #     self.assertEqual(found.func, addActivity)

class HTMLTest(TestCase):
    def test_index_used(self):
        response = self.client.get('/story6/indexStory6/')
        self.assertTemplateUsed(response, 'indexStory6.html')

    def test_addActivity_used(self):
        response = self.client.get('/story6/addActivity/')
        self.assertTemplateUsed(response, 'addActivity.html')

    def test_listActivity_used(self):
        response = self.client.get('/story6/listActivity/')
        self.assertTemplateUsed(response, 'listActivity.html')

class UrlTest(TestCase):
    def test_index_exist(self):
        response = self.client.get('/indexStory6')
        self.assertEqual(response.status_code, 404)

    def test_addActivity_exist(self):
        response = self.client.get('/addActivity')
        self.assertEqual(response.status_code, 404)

    def test_listActivity_exist(self):
        response = self.client.get('/listActivity')
        self.assertEqual(response.status_code, 404)

    def test_registerPerson_exist(self):
        response = self.client.get('/registerPerson')
        self.assertEqual(response.status_code, 404)

# class FormTest(TestCase):
#     def test_form_is_valid(self):
#         activity_form = ActivityForm(data={
#             "activity_name": "nugas",
#             "activity_desc": "Mengerjakan Tugas"
#         })
#         self.assertTrue(activity_form.is_valid())

#         person_form = PersonForm(data={
#             'person_name': "Jaja"
#         })
#         self.assertTrue(person_form.is_valid())
