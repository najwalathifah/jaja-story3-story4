from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('story3.urls')),
    path('story6/', include('story6.urls', namespace='story6'))
]
