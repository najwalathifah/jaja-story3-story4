# from django.forms import ModelForm
from django import forms
from .models import Matkul

class MatkulForm(forms.ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'
        
    courseName = forms.CharField(label='Course Name', widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_courseName'}))
    lecturerName = forms.CharField(label='Lecturer Name', widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_lecturerName'}))
    sks = forms.IntegerField(label='SKS', widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_sks'}))
    courseDesc = forms.CharField(label='Description', widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_courseDesc'}))
    term = forms.CharField(label='Term', widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_term'}))
    room = forms.CharField(label='Room', widget=forms.TextInput(attrs={'class': 'form-control', 'id': 'id_room'}))