from django.db import models

class Matkul(models.Model):
    courseName = models.CharField(max_length=50)
    lecturerName = models.CharField(max_length=50)
    sks = models.IntegerField()
    courseDesc = models.CharField(max_length=200)
    term = models.CharField(max_length=50)
    room = models.CharField(max_length=50)
    date = models.DateTimeField(auto_now_add=True)