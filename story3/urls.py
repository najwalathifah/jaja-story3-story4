from django.urls import path, include, re_path
from . import views

app_name = 'story3'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('resume/', views.resume, name='resume'),
    path('activity/', views.activity, name='activity'),
    path('story1/', views.story1, name='story1'),
    path('matkul/', views.matkul, name='matkul'),
    path('dataMatkul/', views.dataMatkul, name='dataMatkul'),
    path('delete/<id_matkul>/', views.delMatkul, name='delMatkul'),
]