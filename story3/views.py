from django.shortcuts import render, HttpResponseRedirect
from .forms import MatkulForm
from .models import Matkul

def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def resume(request):
    return render(request, 'resume.html')

def activity(request):
    return render(request, 'activity.html')

def story1(request):
    return render(request, 'story1.html')

def matkul(request):
    form = MatkulForm()

    if request.method == 'POST':
        print(request.POST)
        form = MatkulForm(request.POST)
        
        if form.is_valid():
            Matkul.objects.create(
                courseName = request.POST['courseName'],
                lecturerName = request.POST['lecturerName'],
                sks = request.POST['sks'],
                courseDesc = request.POST['courseDesc'],
                term = request.POST['term'],
                room = request.POST['room'],
            )

            return HttpResponseRedirect('/matkul')
            
    context = {'form':form.as_table}
    return render(request, 'matkul.html', context)

def dataMatkul(request):
    data_matkul = Matkul.objects.all()
    context = {'matkuls':data_matkul}
    return render(request, 'dataMatkul.html', context)

def delMatkul(request, id_matkul):
    Matkul.objects.filter(id=id_matkul).delete()
    return HttpResponseRedirect('/dataMatkul')